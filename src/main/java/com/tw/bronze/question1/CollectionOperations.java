package com.tw.bronze.question1;

import java.util.*;

public class CollectionOperations {
    /**
     * This method will remove all the duplicated `String` values from a
     * collection.
     *
     * @param values The collection which may contain duplicated values
     * @return A new array removing the duplicated values.
     */
    public static String[] distinct(Iterable<String> values) {
        // TODO:
        //  Please implement the method. If you have no idea what Iterable<T>
        //  is, please refer to the unit test, or you can have a search on
        //  the internet, or you can find it in our material.
        // <-start-
        if (values == null) {
            throw new IllegalArgumentException();
        }

        Set<String> set = new HashSet<String>();

        for (String element : values) {
                set.add(element);
        }

        String[] setArray = set.toArray(new String[set.size()]);
        String[] newArray = new String[set.size()];

        if (setArray.length == 0) {
            return setArray;
        }

        for (int i = 0; i <= setArray.length - 1; i++) {
            newArray[i] = setArray[setArray.length - 1 - i];
        }
        return newArray;
        // --end->
    }
}
